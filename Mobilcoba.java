/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.praktikum5;
import java.util.Scanner;
/**
 *
 * @author T I O
 */
public class Mobilcoba {
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        Mobil toyota01 = new Mobil();
        
        toyota01.setModel("Avanza");
        toyota01.setSpeed(120);
        
        System.out.print(" Kecepatan Mobil Sekarang :");
        int kecepatan = input.nextInt();
        
        System.out.print(" Mobil " + toyota01.getModel());
        System.out.print(" mempunyai maxspeed " +toyota01.getSpeed());
        System.out.print(" km per jam\n ");
        
        if (kecepatan >= 120)
            System.out.println("Mobil sudah kencang");
        else
            System.out.println("Mobil belum kencang");
        
    }
    
}
